(function() {
  'use strict';

 angular.module('obd').service('obdService',obdService);

function obdService($resource,ngAuthSettings) {

      var odataUrl = serviceBase + "odata/Company";

    var compurl = serviceBase + "odata/Company";
    return $resource("", {},
    {
		
        'getLastInfo': { method: "GET", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/LastInfo/:key" },
		'getHealthData': { method: "GET", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/HealthData/:key" },
		'getAlerts': { method: "GET", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/MyAlertMessages/:keycomp/:keycat" },
		'getAlertsCount': { method: "GET", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/AlertsCount/:key" },
		
		'getTrips': { method: "GET", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/Trips/:key" },
		'getFaultCodes': { method: "GET", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/FaultCodes/:key" },
		
		'getTripsForDay': { method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/Trips/ByDate" ,
		headers:{ 'Content-Type': 'application/json;odata.metadata=minimal'}},
		
		
		'forget': 	{ 	method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/Account/Forget" ,
						headers: { 'Content-Type': 'application/json;odata.metadata=minimal'}
					},

        'getMyDevices': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/mydevices",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
      'UpdateDevice': {
            method: "PUT", url: ngAuthSettings.apiServiceBaseUri+"api/update/device",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		 'verifypassword': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/verifypassword",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		'addDevice': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/adddevice",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		'changePassword': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/changepass",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		'updateFeedback': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/feedback",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		'changeStatusofAlert': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/MyAlertMessages/view/:devicekey/:eventkey",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		'tripDetails': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/Trips/Details",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		'tripPlayback': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/OBD/Trips/PlayBack",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
		'postPushIDs': {
            method: "POST", url: ngAuthSettings.apiServiceBaseUri+"api/pushnotification/register",
            headers: { 'Content-Type': 'application/json;odata.metadata=minimal' }
        },
          
    });

}})();

