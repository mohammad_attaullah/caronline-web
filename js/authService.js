﻿(function() {
  'use strict';

 angular
    .module('obd').factory('authService',authService);
    function authService($rootScope,$http, $q, localStorageService, ngAuthSettings, $templateCache) {
  var serviceBase = ngAuthSettings.apiServiceBaseUri;
    var authServiceFactory = {};



    var _authentication = {
        isAuth: false,
        userName: "",
        useRefreshTokens: false,
        type: "",
        ID: "",
        ID2:"",
		pushid:'',
		pushtoken:'',
        MobileAccess: false,
        PracticeFusionImport: false,
        IsAdmin: false,
        issued: new Date(),
        expires: new Date(),
        expires_in: 0

    };

  

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/account/Register', registration).then(function (response) {
            return response;
        });

    };
   
    var _login = function (loginData) {
  _logOut();
  $rootScope.selectedUserName= loginData.userName;
  
        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
		loginData.useRefreshTokens=true;
        if (loginData.useRefreshTokens) {
            data = data + "&client_id=" + ngAuthSettings.clientId;
        }
		try{
				var deferred = $q.defer();
		}catch(excep){
			
		}
        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
            var type = "";
            $rootScope.isLogin = true;
          // var expireon = response..expires;
            if (loginData.useRefreshTokens) {
               
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, useRefreshTokens: true, ID: response.ID, me: response.me, role: response.role,email:response.email,ID2: response.ID2 ,pushid:response.pushid,pushtoken:response.pushtoken});
                ID = response.ID;
             
                _authentication.ID= response.ID;
                _authentication.type = response.me;
                _authentication.ID2 = response.role;
				_authentication.email=response.email
                type = response.me;
               
                $rootScope.UserID = loginData.userName;
               
            }
            else {
                var exp = JSON.stringify(response);
                exp = exp.replace(".expire", 'expire');
                exp = exp.replace(".issued", 'issued')
                var v = JSON.parse(exp);
                _authentication.issued = v.issued;
                _authentication.expires = v.expires;
                _authentication.expires_in = v.expires_in;
				_authentication.email=response.email
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false, ID: response.ID, me: response.me, ID2: response.ID2,  issued: _authentication.issued, expires: _authentication.expires, expires_in: _authentication.expires_in,email:response.email,pushid:response.pushid,pushtoken:response.pushtoken });
               


                ID = response.ID;
             
                _authentication.ID = response.ID;
                _authentication.type = response.me;
                _authentication.role = response.role;

             
                type = response.me;
            }
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            _authentication.useRefreshTokens = loginData.useRefreshTokens;
         
            _authentication.type = type;
            $rootScope.UserID = loginData.userName;
            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            // _login(loginData);
            deferred.resolve(err);
        });

        return deferred.promise;

    };

  
    var _logOut = function () {
      //  $templateCache.removeAll();
        localStorageService.remove('authorizationData');
        $rootScope.isLogin = false;
        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.useRefreshTokens = false;
        _authentication.type = "";
        _authentication.ID = null;
        _authentication.ID2 = null;
      
        _authentication.MobileAccess = false;
    
        _authentication.expires = new Date();
        _authentication.expires_in = 0;


       
    };

    var _fillAuthData = function () {
        
   
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            $rootScope.isLogin = true;
            _authentication.userName = authData.userName;
            $rootScope.UserID = authData.userName;
            _authentication.useRefreshTokens = authData.useRefreshTokens;
            _authentication.type = authData.me;
          //  _authentication.ID2 = authData.ID2;
          //  ID2 = authData.ID2;
            _authentication.issued = authData.issued;
            _authentication.expires = authData.expires;
            _authentication.expires_in = authData.expires_in;
			_authentication.email=authData.email

            //me = authData.me;
           
                _authentication.ID = authData.ID;
                ID = authData.ID;
            
            }
            
        }

    

    var _refreshToken = function () {
        var deferred = $q.defer();
		var type = "";
        var authData = localStorageService.get('authorizationData');

        if (authData) {

            if (authData.useRefreshTokens) {

                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + ngAuthSettings.clientId;

               localStorageService.remove('authorizationData');

                $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                  //  localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true, ID: response.ID });
				localStorageService.set('authorizationData', { token: response.access_token, userName: authData.userName, refreshToken: response.refresh_token, useRefreshTokens: true, ID: response.ID, me: response.me, role: response.role,email:response.email ,ID2: response.ID2});
                ID = response.ID;
             
                _authentication.ID= response.ID;
                _authentication.type = response.me;
                _authentication.ID2 = response.role;
				_authentication.email=response.email
                type = response.me;
                 _authentication.isAuth = true;
				_authentication.userName = authData.userName;
				_authentication.useRefreshTokens = true;
				_authentication.type = type;
                $rootScope.UserID = authData.userName;
                deferred.resolve(response);
				$rootScope.loadDataAgain(response.ID);

                }).error(function (err, status) {
                  _logOut();
                    deferred.reject(err);
                });
            }
        }

        return deferred.promise;
    };

    



    authServiceFactory.saveRegistration = _saveRegistration;
  

    authServiceFactory.login = _login;
   
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.refreshToken = _refreshToken;



    return authServiceFactory;
}})();