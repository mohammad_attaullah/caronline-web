// Ionic obd App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'obd' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'obd.controllers' is found in controllers.js

var serviceBase = 'http://211.24.114.181:99/';//'http://175.136.243.117:99/'
//var serviceBase='http://localhost:26264/';//
var ID;
angular.module('obd', ['ionic', 'obd.controllers', 'ngCordova', 'pascalprecht.translate', 'ngResource', 'LocalStorageModule', "firebase", 'ngMap', 'ngMaterial'])
  .run(['authService', function (authService) {
    authService.fillAuthData();
  }])
  .run(function ($rootScope, $ionicPlatform, $ionicHistory, obdService, ngAuthSettings) {


    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
      if (navigator.splashscreen) {
        navigator.splashscreen.hide();
      }
      var notificationOpenedCallback = function (jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

    });

    /*exit button code*/

    $ionicPlatform.registerBackButtonAction(function (e) {
      if ($rootScope.backButtonPressedOnceToExit) {
        ionic.Platform.exitApp();
      }

      else if ($ionicHistory.backView()) {
        $ionicHistory.goBack();
      }
      else {
        $rootScope.backButtonPressedOnceToExit = true;
        window.plugins.toast.showShortCenter(
          "Press back button again to exit", function (a) { }, function (b) { }
        );
        setTimeout(function () {
          $rootScope.backButtonPressedOnceToExit = false;
        }, 2000);
      }
      e.preventDefault();
      return false;
    }, 101);

  })


  .config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
  }])

  .constant('ngAuthSettings', {

    //apiServiceBaseUri:'http://localhost:26264/',
    apiServiceBaseUri: 'http://211.24.114.181:99/',
    clientId: 'app.carOnline.Guru',
    ProviderID: 1,
    PracticeID: 1,
    PracticeLocationID: 1,
    push: {},
    payPalSandboxId: 'EKLVodY1fM0d3GYkQJ_7Djp5qsyqErerHXtiSvOWLIAc7EDcH6PkqdZCH2XB-QzSMc8BFWMgwOr-GotH',
    payPalProductionId: 'production id here',
    payPalEnv: 'PayPalEnvironmentSandbox',   // for testing  production for production
    payPalShopName: 'Car Online Demo',
    payPalMerchantPrivacyPolicyURL: 'http://www.caronline.guru/privacypolicy',
    payPalMerchantUserAgreementURL: 'http://www.caronline.guru/agriment'
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        /*abstract: true,*/
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.search', {
        url: '/search',
        views: {
          'menuContent': {
            templateUrl: 'templates/search.html'
          }
        }
      })

      .state('app.browse', {
        url: '/browse',
        views: {
          'menuContent': {
            templateUrl: 'templates/browse.html'
          }
        }
      })
      .state('app.root', {
        url: '/root',
        views: {
          'menuContent': {
            templateUrl: 'templates/root.html',
            controller: 'RootsCtrl'
          }
        }
      })
      .state('app.trip', {
        url: '/trip',
        views: {
          'menuContent': {
            templateUrl: 'templates/trip.html',
            controller: 'TripCtrl'
          }
        }
      })

      .state('app.personal-center', {
        url: '/personal-center',
        views: {
          'menuContent': {
            templateUrl: 'templates/personal-center.html',
            controller: 'PersonalCenterCtrl'
          }
        }
      })
      .state('app.settings', {
        url: '/settings',
        views: {
          'menuContent': {
            templateUrl: 'templates/settings.html',
            controller: 'SettingsCtrl'
          }
        }
      })
      .state('app.alerts', {
        url: '/alerts',
        views: {
          'menuContent': {
            templateUrl: 'templates/AlertsMain.html',
            controller: 'AlertCtrl'
          }
        }
      }).state('app.AlertDetail', {
        url: '/AlertDetail',
        views: {
          'menuContent': {
            templateUrl: 'templates/AlertsDetails.html',
            controller: 'AlertCtrl'
          }
        }
      })
      .state('app.trip-data', {
        url: '/trip-data',
        views: {
          'menuContent': {
            templateUrl: 'templates/trip-data.html',
            controller: 'TripCtrl'
          }
        }
      })


      .state('app.driver-info', {
        url: '/driver-info',
        views: {
          'menuContent': {
            templateUrl: 'templates/DriverInfo.html',
            controller: 'PersonalCenterCtrl'
          }
        }
      })
      .state('app.custom-reminder', {
        url: '/custom-reminder',
        views: {
          'menuContent': {
            templateUrl: 'templates/customReminder.html',

          }
        }
      }).state('app.management-reminder', {
        url: '/management-reminder',
        views: {
          'menuContent': {
            templateUrl: 'templates/ManagementReminder.html',

          }
        }
      }).state('app.change-password', {
        url: '/change-password',
        views: {
          'menuContent': {
            templateUrl: 'templates/changePassword.html',

          }
        }
      }).state('app.signup', {
        url: '/signup',
        views: {
          'menuContent': {
            templateUrl: 'templates/signup.html',
            controller: 'signupController'

          }
        }
      })
      .state('app.license', {
        url: '/license',
        views: {
          'menuContent': {
            templateUrl: 'templates/paymentPage.html',
            controller: 'PaymentCtrl'
          }
        }
      })
      .state('app.googleMap', {
        url: '/googleMap',
        views: {
          'menuContent': {
            templateUrl: 'templates/GoogleMapRoute.html',
            controller: 'GoogleCtrl'
          }
        }
      })
      /*.state('app.googleMapAll', {
          url: '/googleMap',
          views: {
            'menuContent': {
              templateUrl: 'templates/GoogleMapRouteAll.html',
        controller:'GoogleCtrl'
            }
          }
        })*/
      .state('app.baiduMap', {
        url: '/baiduMap',
        views: {
          'menuContent': {
            templateUrl: 'templates/BaiduMapRoute.html',
            controller: 'Baidu2Ctrl'
          }
        }
      })
      .state('app.baiduMapRoute', {
        url: '/baiduMap',
        views: {
          'menuContent': {
            templateUrl: 'templates/BaiduMapGeneral.html',
            controller: 'BaiduCtrl'
          }
        }
      })
      .state('app.testPayment', {
        url: '/testPayment',
        views: {
          'menuContent': {
            templateUrl: 'templates/paypal.html',
            controller: 'PaymentCtrl'
          }
        }
      })

      .state('app.device-info', {
        url: '/device-info',
        views: {
          'menuContent': {
            templateUrl: 'templates/deviceinfo.html',

          }
        }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/root');
  }).config(function ($translateProvider) {

    $translateProvider.preferredLanguage('en');
    $translateProvider.translations('en', {
      'EMAIL': 'Email',
      'LOGIN': 'Login',
      'LOGOUT': 'Logout',
      'PASSWORD': 'Password',
      'PLEASE_WAIT': 'Please Wait',
      'SAVE': 'Save',
      'SCHEDULE_E_APPOINTMENT': 'Schedule E-Appointment',
      'SCHEDULE_eCONSULTANT_WITH_THE_DOCTOR': 'Schedule Econsultant With The Doctor',
      'SEARCH': 'Search',
      'SIGN_UP': 'Register',
      'STATE': 'State',
      'USER_ID': 'User Id',
      'BACK': 'back',
      'STOP': 'Stop',
      'MESSAGE': 'message',
      'REMEMBERME': 'Remember',
      'CONFIRM': 'Confirm',
      'SUBMIT': 'Submit',
      'NAME': 'Name',
      'Close': 'Close',
      'LANGUAGE': 'Language',
      'SCAN': 'Scan',
      'CONFIRM_PASSWORD': 'Confirm Password',
      'SIGN_UP_FORM': 'Sign Up Form',
      'MESSAGES': 'Messages',
      'CALL': 'Call',
      'NEW': 'New ',
      ' FAILED': 'Failed to register user due to',
      'EMAIL_RRQIRED': 'Email is required',
      'EMAIL_EXIST': 'This email is already subscribed please try different email id',
      'PASSWORD_MINIUM_CHAR': 'Minimum 8 characters required.',
      'PASSWORD_MISSMATCH': 'Password mismatch please enter correct password',
      'NO': 'No',
      'YES': 'Yes',
      'CANCEL': 'Cancel',
      'DEVICE_NO': 'Device No.',
      'Device_Information': 'Device Information',
      'Driver_Information': 'Driver Information',
      'Driver': 'Driver',
      'Name': 'Name',
      'ID_No': 'ID No',
      'DOB': 'DOB',
      'License_Information': 'License Information',
      'Type': 'Type',
      'License_Type': 'License Type',
      'License_Number': 'License Number',
      'Number': 'Number',
      'Exp_Date': 'Exp. Date',
      'Camera': 'Camera',
      'Gallery': 'Gallery',
      'General': 'General',
      'Registration_No': 'Registration No',
      'Make': 'Make',
      'Model': 'Model',
      'Year': 'Year',
      'Color': 'Color',
      'Insurance': 'Insurance',
      'Company': 'Company',
      'Maintinence': 'Maintinence',
      'Reading': 'Reading',
      'Interval': 'Interval',
      'Next': 'Next',
      'Personal Center': 'Personal Center',
      'Vechine_info': 'Vehicle info',
      'Driver info': 'Driver info',
      'Customized reminder': 'Customized reminder',
      'Maintinence reminders': 'Maintinence reminders',
      'Electronic fence': 'Electronic fence',
      'Speeding': 'Speeding',
      'Ideling': 'Idling',
      'Reports': 'Reports',
      'POIs': '"POIs"',
      'Email notification': 'Email notification',
      'license renewal': 'license renewal',
      'My Car': 'My Car',
      'Trip': 'Trip',
      'Settings': 'Settings',
      'Off time usage privacy protection': 'Off time usage privacy protection',
      'Account Information': 'Account Information',
      'Change the password': 'Change the password',
      'Activity log': 'Activity log',
      'Feedback': 'Feedback',
      'About us': 'About us',
      'Help': 'Help',
      'Software upgrader': 'Software upgrader',
      'Log out': 'Log out',
      'Travel Time': 'Total Travel',
      'Total mileage': 'Total mileage',
      'Total Trips': 'Total Trips',
      'Total Fuel': 'Total Fuel',
      'Trip Data': 'Trip Data',
      'Starting Time': 'Starting Time',
      'Ending Time': 'Ending Time',
      'Check the travel path': 'Check the travelled path',
      'Time': 'Time',
      'Distance': 'Distance',
      'Total fule usage': 'Total fuel usage',
      'Fuel consumption': 'Fuel consumption',
      'Avg Speed': 'Avg Speed',
      'Max Speed': 'Max Speed',
      'Max water Temp': 'Max water Temp',
      'Engine revolution': 'Engine revolution',
      'Driving habits Analysis': 'Driving habits Analysis',
      'Over speed duration': 'Over speed duration',
      'Fatigue driving duration': 'Fatigue driving duration',
      'High G force instance': 'High G force instance',
      'Harsh braking instance': 'Harsh braking instance',
      'Acceleration instance': 'Acceleration instance',
      'Harsh acceleration instance': 'Harsh acceleration instance',
      'CarOnline.Guru': 'CarOnline.Guru',
      'Check': 'Check',
      'Location': 'Location',
      'Avg oil consume': 'Avg fuel consumption',
      'Number driving': 'No. of trips',
      'Next maintenance': 'Next maintenance',
      'Health Check': 'Health Check',
      'Retry': 'Retry',
      'Description': 'Description',
      'Value': 'Value',
      'Range': 'Range',
      'Username': 'User Name',
      'password': 'password',
      'Account Information': 'Account Information',
      'Old': 'Old',
      'New': 'New',
      'Old Password': 'Old Password',
      'Confirm Pasword': 'Confirm Pasword',
      'Kilometer': 'Kilometer',
      'Liter': 'Liter',
      'Average RPM': 'Average RPM',
      'Maximum RPM': 'Maximum RPM',
      'Average throttle position': 'Average throttle position',
      'Maximun throttle position': 'Maximun throttle position',
      'Average engine load': 'Average engine load',
      'Maximum engine load': 'Maximum engine load',
      'Driving habits analysis': 'Driving habits analysis',
      'Crash incident': 'Crash incident',
      'Harsh turn': 'Harsh turn',
      'Harsh break and turn': 'Harsh break and turn',
      'Harsh acceleration and turn': 'Harsh Acc. & Turn',
      'Unknown harsh': 'Unknown harsh',
      'General information': 'General information',
      'Insurance information': 'Insurance information',
      'Maintinence information': 'Maintinence information',
      'Edit': 'Edit',
      'Calcel': 'Calcel',
      'Verification': 'Verification',
      'Enter password for user': 'Enter password for user',
      'Select source': 'Select source',
      'Camera': 'Camera',
      'Files': 'Files',
      'Driver information': 'Driver information',
      'Speeding alert': 'Speeding alert',
      'Ideling alert': 'Ideling alert',
      'Reminders': 'Reminders',
      'Management reminder': 'Management reminder',
      'Insurance expiry alert': 'Insurance expiry alert',
      'Driving license expiry alert': 'Driving license expiry alert',
      'Account expiry alert': 'Account expiry alert',
      'Maintinence alrt': 'Maintinence alrt',
      'Please upgrade your package to Plan-4': 'Please upgrade your package to Plan-4',
      'ok': 'ok',
      'Business and private usage': 'Business and private usage',
      'Dear customer': 'Dear customer',
      'This app comes with one month free license from date of registration. Please take note of expiry date and renew your license in time to avoid service interruption.': 'This app comes with one month free license from date of registration. Please take note of expiry date and renew your license in time to avoid service interruption.',
      'Plan 1': 'Plan 1',
      'Plan 2': 'Plan 2',
      'Plan 3': 'Plan 3',
      'Plan 4': 'Plan 4',
      '1 month': '1 month',
      '6 month': '6 month',
      '1 year': '1 year',
      'Please note that any plan selected cannot be changed or modified once payment is done until the expiry date': 'Please note that any plan selected cannot be changed or modified once payment is done until the expiry date',
      'Trial account': 'Trial account',
      'Plan': 'Plan',
      'New plan': 'New plan',
      'Total': 'Total',
      'Proceed to pay': 'Proceed to pay',
      'Email': 'Email',
      'New password': 'New password',
      'Map settings': 'Map settings',
      'Google map': 'Google map',
      'Baidu map': 'Baidu map',
      'You are welcomed to put forward valueable openions and suggestions': 'You are welcomed to put forward valueable openions and suggestions',
      'Enter your comments and suggestions': 'Enter your comments and suggestions',
      'Submit Feedback': 'Submit Feedback',
      'Mobile App for GPS enabled devices': 'Mobile App for GPS enabled devices',
      'Complete control of the usage and condition of your vehicle': 'Complete control of the usage and condition of your vehicle',
      'Version': 'Version',
      'Confirmation': 'Confirmation',
      'Are you sure to logout': 'Are you sure to logout',
      'Yes': 'Yes',
      'No': 'No',



    });


    $translateProvider.translations('cn', {
      'EMAIL': '电子邮件',
      'LOGIN': '登录',
      'LOGOUT': '登出',
      'PASSWORD': '密码',
      'PLEASE_WAIT': '请稍候',
      'SAVE': '保存',
      'SCHEDULE_E_APPOINTMENT': '附表E-约会',
      'SCHEDULE_eCONSULTANT_WITH_THE_DOCTOR': '附表Econsultant与医生',
      'SEARCH': '搜索',
      'SIGN_UP': '注册',
      'STATE': '州',
      'USER_ID': '用户名',
      'BACK': '背部',
      'STOP': '停止',
      'MESSAGE': '信息',
      'REMEMBERME': '记得',
      'CONFIRM': '确认',
      'SUBMIT': '提交',
      'NAME': '名称',
      'Close': '关',
      'LANGUAGE': '语言',
      'SCAN': '扫描',
      'CONFIRM_PASSWORD': '确认密码',
      'SIGN_UP_FORM': '注册表格',
      'MESSAGES': '消息',
      'CALL': '呼叫',
      'NEW': '新',
      ' FAILED': '无法注册用户因',
      'EMAIL_RRQIRED': '电子邮件是必需的',
      'EMAIL_EXIST': '此电子邮件已订阅请尝试不同的电子邮件ID',
      'PASSWORD_MINIUM_CHAR': '最少需要8个字符。',
      'PASSWORD_MISSMATCH': '密码不匹配，请输入正确的密码',
      'NO': '没有',
      'YES': '是',
      'CANCEL': '取消',
      'DEVICE_NO': '设备编号',
      'Device_Information': '设备信息',
      'Driver_Information': '驱动程序信息',
      'Driver': '司机',
      'Name': '名称',
      'ID_No': '证件号码',
      'DOB': 'DOB',
      'License_Information': '许可信息',
      'Type': '类型',
      'License_Type': '许可证类型',
      'License_Number': '许可证号',
      'Number': '数',
      'Exp_Date': '进出口。日期',
      'Camera': '相机',
      'Gallery': '画廊',
      'General': '一般',
      'Registration_No': '注册号',
      'Make': '使',
      'Model': '模型',
      'Year': '年',
      'Color': '颜色',
      'Insurance': '保险',
      'Company': '公司',
      'Maintinence': 'Maintinence',
      'Reading': '读',
      'Interval': '间隔',
      'Next': '下一个',
      'Personal Center': '个人中心',
      'Vechine_info': 'Vechine信息',
      'Driver info': '驱动程序信息',
      'Customized reminder': '定制提醒',
      'Maintinence reminders': 'Maintinence提醒',
      'Electronic fence': '电子围栏',
      'Speeding': '超速',
      'Ideling': 'Ideling',
      'Reports': '报告',
      'POIs': '兴趣点',
      'Email notification': '电子邮件通知',
      'license renewal': '牌照续期',
      'My Car': '我的车',
      'Trip': '旅',
      'Settings': '设置',
      'Off time usage privacy protection': '关机时间的使用隐私保护',
      'Account Information': '帐户信息',
      'Change the password': '更改密码',
      'Activity log': '活动日志',
      'Feedback': '反馈',
      'About us': '关于我们',
      'Help': '帮帮我',
      'Software upgrader': '软件升级程序',
      'Log out': '登出',
      'Travel Time': '旅行时间',
      'Total mileage': '总里程',
      'Total Trips': '总行程',
      'Total Fule ': '共富乐',
      'Trip Data': '旅行数据',
      'Starting Time': '开始时间',
      'Ending Time': '结束时间',
      'Check the travel path': '检查行进路径',
      'Time': '时间',
      'Distance': '距离',
      'Total fule usage': '共富乐用法',
      'Fuel consumption': '燃油消耗',
      'Avg Speed': '平均速度',
      'Max Speed': '最大速度',
      'Max water Temp': '最高水温',
      'Engine revolution': '发动机转数',
      'Driving habits Analysis': '驾驶习惯分析',
      'Over speed duration': '超速时间',
      'Fatigue driving duration': '疲劳驾驶时间',
      'High G force instance': '高G力实例',
      'Harsh braking instance': '刺耳的刹车情况',
      'Acceleration instance': '例如加速度',
      'Harsh acceleration instance': '恶劣的加速实例',
      'CarOnline.Guru': 'CarOnline.Guru',
      'Check': '检查',
      'Location': '位置',
      'Avg oil consume': '平均油耗',
      'Number driving': '一批带动',
      'Next maintenance': '下一步维修',
      'Health Check': '健康检查',
      'Retry': '重试',
      'Description': '描述',
      'Value': '值',
      'Range': '范围',
      'Username': '用户名',
      'password': '密码',
      'Account Information': '帐户信息',
      'Old': '旧',
      'New': '新',
      'Old Password': '旧密码',
      'Confirm Pasword': '确认Pasword',
      'Kilometer': '公里',
      'Liter': '公升',
      'Average RPM': '平均转速',
      'Maximum RPM': '最高转速',
      'Average throttle position': '平均油门位置',
      'Maximun throttle position': '最高油门位置',
      'Average engine load': '平均发动机负载',
      'Maximum engine load': '最高发动机负载',
      'Driving habits analysis': '驾驶习惯分析',
      'Crash incident': '碰撞事故',
      'Harsh turn': '恶劣拐弯',
      'Harsh break and turn': '恶劣刹车与拐弯',
      'Harsh acceleration and turn': '恶劣加速与拐弯',
      'Unknown harsh': '未知的恶劣驾驶',
      'General information': '一般信息',
      'Insurance information': '保险信息',
      'Maintinence information': '保养信息',
      'Edit': '编辑',
      'Calcel': '取消',
      'Verification': '确认',
      'Enter password for user': '请输入用户密码',
      'Select source': '选择来源',
      'Camera': '相机',
      'Files': '文档',
      'Driver information': '驾驶员信息',
      'Speeding alert': '超速提醒',
      'Ideling alert': '空转提醒',
      'Reminders': '提醒',
      'Management reminder': '管理提醒',
      'Insurance expiry alert': '保险到期提醒',
      'Driving license expiry alert': '驾照到期提醒',
      'Account expiry alert': '账号到期提醒',
      'Maintinence alrt': '保养提醒',
      'Please upgrade your package to Plan-4': '请升级到4号套餐',
      'ok': 'OK',
      'Business and private usage': '商务和私人使用',
      'Dear customer': '亲爱的客户',
      'This app comes with one month free license from date of registration. Please take note of expiry date and renew your license in time to avoid service interruption.': '这个APP已含，从注册日算起，一个月的使用许可证。请注意许可证的到期日期，即时更新许可证以免服务受到干扰。',
      'Plan 1': '1号套餐',
      'Plan 2': '2号套餐',
      'Plan 3': '3号套餐',
      'Plan 4': '4号套餐',
      '1 month': '1个月',
      '6 month': '6个月',
      '1 year': '1年',
      'Please note that any plan selected cannot be changed or modified once payment is done until the expiry date': '请注意：已选并付费完成的套餐不能改变或修改。',
      'Trial account': '使用账号',
      'Plan': '套餐',
      'New plan': '新套餐',
      'Total': '总金额',
      'Proceed to pay': '继续支付',
      'Email': '邮件',
      'New password': '新密码',
      'Map settings': '地图设置',
      'Google map': '谷歌地图',
      'Baidu map': '百度地图',
      'You are welcomed to put forward valueable openions and suggestions': '我们欢饮你提供宝贵的意见与建议。',
      'Enter your comments and suggestions': '请输入你的评论与建议。',
      'Submit Feedback': '上传反馈',
      'Mobile App for GPS enabled devices': '有GPS功能的移动终端APP',
      'Complete control of the usage and condition of your vehicle': '完全掌控车辆状况及使用情况',
      'Version': '版本',
      'Confirmation': '确认',
      'Are you sure to logout': '你真的要退出吗',
      'Yes': '是',
      'No': '否',



    });
    $translateProvider.translations('ml', {
      'EMAIL': 'e-mel',
      'LOGIN': 'Log masuk',
      'LOGOUT': 'Log keluar',
      'PASSWORD': 'kata laluan',
      'PLEASE_WAIT': 'Sila tunggu',
      'SAVE': 'Simpan',
      'SCHEDULE_E_APPOINTMENT': 'Jadual E-Temujanji',
      'SCHEDULE_eCONSULTANT_WITH_THE_DOCTOR': 'Jadual eRunding Dengan Doktor',
      'SEARCH': 'carian',
      'SIGN_UP': 'Daftar',
      'STATE': 'Negeri',
      'USER_ID': 'ID Pengguna',
      'BACK': 'Undur',
      'STOP': 'Henti',
      'MESSAGE': 'Pesanan Ringkas',
      'REMEMBERME': 'Ingat',
      'CONFIRM': 'sahkan',
      'SUBMIT': 'hantar',
      'NAME': 'Nama',
      'Close': 'Tutup',
      'LANGUAGE': 'bahasa',
      'SCAN': 'Imbas',
      'CONFIRM_PASSWORD': 'sahkan Kata laluan',
      'SIGN_UP_FORM': ' Borang Pendaftaran',
      'MESSAGES': 'Pesanan Ringkas',
      'CALL': 'Hubungi',
      'NEW': 'Baru',
      ' FAILED': 'Pendaftaran Pengguna Gagal kerana',
      'EMAIL_RRQIRED': 'E-mel diperlukan',
      'EMAIL_EXIST': 'e-mel ini telah melanggan sila cuba id e-mel yang berbeza',
      'PASSWORD_MINIUM_CHAR': 'Minimum 8 aksara diperlukan.',
      'PASSWORD_MISSMATCH': 'Kata laluan tidak sepadan sila masukkan kata laluan yang betul',
      'NO': 'Tidak',
      'YES': 'Ya',
      'CANCEL': 'Batal',
      'DEVICE_NO': 'No. peranti',
      'Device_Information': 'Maklumat peranti',
      'Driver_Information': 'Maklumat pemandu',
      'Driver': 'pemandu',
      'Name': 'Nama',
      'ID_No': 'No ID',
      'DOB': 'DOB',
      'License_Information': 'Maklumat lesen',
      'Type': 'Jenis',
      'License_Type': 'Jenis lesen',
      'License_Number': 'Nombor lesen',
      'Number': 'Nombor',
      'Exp_Date': 'Tarikh Luput',
      'Camera': 'Kamera',
      'Gallery': 'Galeri',
      'General': 'Umum',
      'Registration_No': 'No pendaftaran',
      'Make': 'Buatan',
      'Model': 'model',
      'Year': 'tahun',
      'Color': 'warna',
      'Insurance': 'Insuran',
      'Company': 'syarikat',
      'Maintinence': 'Selenggara',
      'Reading': 'sedang membaca',
      'Interval': 'Selang',
      'Next': 'Seterusnya',
      'Personal Center': 'Pusat peribadi',
      'Vechine_info': 'maklumat Kenderaan',
      'Driver info': 'maklumat pemandu',
      'Customized reminder': 'peringatan disesuaikan',
      'Maintinence reminders': 'peringatan selenggara',
      'Electronic fence': 'pagar elektronik',
      'Speeding': 'memandu laju',
      'Ideling': 'melahu',
      'Reports': 'laporan',
      'POIs': 'POI',
      'Email notification': 'pemberitahuan e-mel',
      'license renewal': 'pembaharuan lesen',
      'My Car': 'Kereta saya',
      'Trip': 'Perjalanan',
      'Settings': 'Tetapan',
      'Off time usage privacy protection': 'perlindungan privasi pengguna telah tamat',
      'Account Information': 'Maklumat Akaun',
      'Change the password': 'Tukar kata laluan',
      'Activity log': 'Log aktiviti',
      'Feedback': 'Maklumbalas',
      'About us': 'Tentang kami',
      'Help': 'Bantuan',
      'Software upgrader': 'Naik taraf perisian',
      'Log out': 'Log keluar',
      'Travel Time': 'Masa perjalanan',
      'Total mileage': 'Jumlah perbatuan',
      'Total Trips': 'Jumlah Perjalanan',
      'Total Fule ': 'Jumlah Bahan Api',
      'Trip Data': 'Data Perjalanan',
      'Starting Time': 'Masa bermula',
      'Ending Time': 'Masa Tamat',
      'Check the travel path': 'Semak laluan perjalanan',
      'Time': 'Masa',
      'Distance': 'Jarak',
      'Total fule usage': 'Jumlah penggunaan Bahan api',
      'Fuel consumption': 'Penggunaan bahan api',
      'Avg Speed': 'Purata Kelajuan',
      'Max Speed': 'Had Laju Maksima',
      'Max water Temp': 'Takat Suhu air maksima',
      'Engine revolution': 'Revolusi Enjin',
      'Driving habits Analysis': 'Analisa Tabiat memandu',
      'Over speed duration': 'Had Laju melebihi tempoh',
      'Fatigue driving duration': 'Keletihan dalam Tempoh memandu',
      'High G force instance': 'Kuasa daya G mendadak',
      'Harsh braking instance': 'membrek secara keras',
      'Acceleration instance': 'pecutan mendadak',
      'Harsh acceleration instance': 'memecut secara keras',
      'CarOnline.Guru': 'CarOnline.Guru',
      'Check': 'Periksa',
      'Location': 'lokasi',
      'Avg oil consume': 'Purata penggunaan Bahan Api',
      'Number driving': 'bilangan memandu',
      'Next maintenance': 'penyelenggaraan seterusnya',
      'Health Check': 'Pemeriksaan kesihatan',
      'Retry': 'masuk semula',
      'Description': 'Huraian',
      'Value': 'nilai',
      'Range': 'pelbagai',
      'Username': 'Nama pengguna',
      'password': 'kata laluan',
      'Account Information': 'Maklumat Akaun',
      'Old': 'Lama',
      'New': 'Baru',
      'Old Password': 'kata laluan lama',
      'Confirm Pasword': 'sahkan Kata laluan',
      'Kilometer': 'kilometer',
      'Liter': 'Liter',
      'Average RPM': 'Purata RPM',
      'Maximum RPM': 'RPM Maksimum',
      'Average throttle position': 'Purata kedudukan pendikit',
      'Maximun throttle position': 'Kedudukan Pendikit Maksimum',
      'Average engine load': 'Purata beban Enjin',
      'Maximum engine load': 'Beban Enjin Maksimum',
      'Driving habits analysis': 'Analisa Tabiat memandu',
      'Crash incident': 'Kemalangan',
      'Harsh turn': 'membelok secara keras',
      'Harsh break and turn': 'brek dan membelok secara keras',
      'Harsh acceleration and turn': 'pecut dan membelok secara keras',
      'Unknown harsh': 'mengejut tanpa sebab',
      'General information': 'Maklumat Umum',
      'Insurance information': 'Maklumat Insuran',
      'Maintinence information': 'Maklumat Penyelenggaraan',
      'Edit': 'Sunting',
      'Calcel': 'Batal',
      'Verification': 'Pengesahan',
      'Enter password for user': 'Masukkan katalaluan pengguna',
      'Select source': 'pilih sumber',
      'Camera': 'kamera',
      'Files': 'fail',
      'Driver information': 'maklumat pemandu',
      'Speeding alert': 'amaran had laju',
      'Ideling alert': 'amaran melahu',
      'Reminders': 'peringatan',
      'Management reminder': 'pengurusan peringatan',
      'Insurance expiry alert': 'amaran tamat tempoh insuran',
      'Driving license expiry alert': 'amaran tamat tempoh lesen memandu',
      'Account expiry alert': 'amaran tamat tempoh akaun',
      'Maintinence alrt': 'Amaran penyelenggaraan',
      'Please upgrade your package to Plan-4': 'sila naik taraf pakej anda ke plan 4',
      'ok': 'ok',
      'Business and private usage': 'penggunaan bisnes and persendirian',
      'Dear customer': 'Pelangan yang dikasihi',
      'This app comes with one month free license from date of registration. Please take note of expiry date and renew your license in time to avoid service interruption.': 'Aplikasi ini disertakan bersama satu bulan lesen percuma dari tarikh pendaftaran. Sila ambil perhatian sekiranya tamat tempoh dan perbaharui lesen anda untuk menghindari dari gangguan perkhidmatan.',
      'Plan 1': 'plan 1',
      'Plan 2': 'plan 2',
      'Plan 3': 'plan 3',
      'Plan 4': 'plan 4',
      '1 month': '1 bulan',
      '6 month': '6 bulan',
      '1 year': '1 tahun',
      'Please note that any plan selected cannot be changed or modified once payment is done until the expiry date': 'Sila ambil perhatian, setiap plan yang dipilih tidak boleh ditukar atau diubahsuai setelah pembayaran dibuat sehingga tamat tempoh.',
      'Trial account': 'akaun percubaan',
      'Plan': 'plan',
      'New plan': 'plan baru',
      'Total': 'keseluruhan',
      'Proceed to pay': 'teruskan pembayaran',
      'Email': 'e-mel',
      'New password': 'katalaluan baru',
      'Map settings': 'tetapan peta',
      'Google map': 'peta google',
      'Baidu map': 'peta baidu',
      'You are welcomed to put forward valueable openions and suggestions': 'sebarang pendapat dan cadangan anda adalah amat dihargai',
      'Enter your comments and suggestions': 'masukkan pendapat dan cadangan anda',
      'Submit Feedback': 'hantar maklum balas',
      'Mobile App for GPS enabled devices': 'Aplikasi mudah alih GPS untuk peranti yang dibenarkan',
      'Complete control of the usage and condition of your vehicle': 'kawalan',
      'Version': 'versi',
      'Confirmation': 'Pengesahan',
      'Are you sure to logout': 'anda pasti untuk keluar',
      'Yes': 'ya',
      'No': 'tidak',



    });

    $translateProvider.translations('ar', {
      'EMAIL': 'البريد الإلكتروني',
      'LOGIN': 'تسجيل الدخول',
      'LOGOUT': 'خروج',
      'PASSWORD': 'كلمه السر',
      'PLEASE_WAIT': 'أرجو الإنتظار',
      'SAVE': 'حفظ',
      'SCHEDULE_E_APPOINTMENT': 'جدول E-تعيين',
      'SCHEDULE_eCONSULTANT_WITH_THE_DOCTOR': 'جدول Econsultant مع الطبيب',
      'SEARCH': 'بحث',
      'SIGN_UP': 'سجل',
      'STATE': 'حالة',
      'USER_ID': 'معرف المستخدم',
      'BACK': 'الى الخلف',
      'STOP': 'توقف',
      'MESSAGE': 'الرسالة',
      'REMEMBERME': 'تذكر',
      'CONFIRM': 'أكد',
      'SUBMIT': 'عرض',
      'NAME': 'اسم',
      'Close': 'قريب',
      'LANGUAGE': 'لغة',
      'SCAN': 'تفحص',
      'CONFIRM_PASSWORD': 'تأكيد كلمة المرور',
      'SIGN_UP_FORM': 'توقيع نموذج أعلى',
      'MESSAGES': 'رسائل',
      'CALL': 'مكالمة',
      'NEW': 'جديد',
      ' FAILED': 'فشل فى تسجيل مستخدم بسبب',
      'EMAIL_RRQIRED': 'مطلوب البريد الإلكتروني',
      'EMAIL_EXIST': 'واشتركت هذه الرسالة بالفعل من فضلك حاول مختلفة معرف البريد الإلكتروني',
      'PASSWORD_MINIUM_CHAR': 'الحد الأدنى 8 أحرف المطلوبة.',
      'PASSWORD_MISSMATCH': 'عدم تطابق كلمة المرور يرجى إدخال كلمة المرور الصحيحة',
      'NO': 'لا',
      'YES': 'نعم فعلا',
      'CANCEL': 'إلغاء',
      'DEVICE_NO': 'جهاز رقم',
      'Device_Information': 'معلومات الجهاز',
      'Driver_Information': 'معلومات السائق',
      'Driver': 'سائق',
      'Name': 'اسم',
      'ID_No': 'رقم بطاقة الهوية',
      'DOB': 'تاريخ الميلاد',
      'License_Information': 'معلومات الترخيص',
      'Type': 'اكتب',
      'License_Type': 'نوع الرخصة',
      'License_Number': 'رقم الرخصة',
      'Number': 'عدد',
      'Exp_Date': 'إكسب. تاريخ',
      'Camera': 'الة تصوير',
      'Gallery': 'صالة عرض',
      'General': 'جنرال لواء',
      'Registration_No': 'رقم التسجيل',
      'Make': 'صنع',
      'Model': 'نموذج',
      'Year': 'عام',
      'Color': 'اللون',
      'Insurance': 'تأمين',
      'Company': 'شركة',
      'Maintinence': 'Maintinence',
      'Reading': 'قراءة',
      'Interval': 'فترة',
      'Next': 'التالى',
      'Personal Center': 'مركز الشخصية',
      'Vechine_info': 'معلومات Vechine',
      'Driver info': 'معلومات السائق',
      'Customized reminder': 'تذكير مخصصة',
      'Maintinence reminders': 'تذكير Maintinence',
      'Electronic fence': 'سياج الكتروني',
      'Speeding': 'سريع',
      'Ideling': 'Ideling',
      'Reports': 'تقارير',
      'POIs': 'النقاط المهمة',
      'Email notification': 'ملاحظات الايميل',
      'license renewal': 'تجديد الترخيص',
      'My Car': 'سيارتي',
      'Trip': 'رحلة',
      'Settings': 'إعدادات',
      'Off time usage privacy protection': 'قبالة الوقت حماية الخصوصية الاستخدام',
      'Account Information': 'معلومات الحساب',
      'Change the password': 'تغيير كلمة المرور',
      'Activity log': 'سجل النشاطات',
      'Feedback': 'ردود الفعل',
      'About us': 'معلومات عنا',
      'Help': 'مساعدة',
      'Software upgrader': 'upgrader البرمجيات',
      'Log out': 'خروج',
      'Travel Time': 'وقت السفر',
      'Total mileage': 'إجمالي عدد الكيلومترات',
      'Total Trips': 'مجموع رحلات',
      'Total Fule ': 'إجمالي فول',
      'Trip Data': 'بيانات الرحلة',
      'Starting Time': 'وقت البدء',
      'Ending Time': 'إنهاء الوقت',
      'Check the travel path': 'تحقق من مسار السفر',
      'Time': 'مرة',
      'Distance': 'المسافات',
      'Total fule usage': 'إجمالي استخدام فول',
      'Fuel consumption': 'استهلاك الوقود',
      'Avg Speed': 'السرعة المتوسطة',
      'Max Speed': 'السرعة القصوى',
      'Max water Temp': 'ماكس درجة الحرارة المياه',
      'Engine revolution': 'الثورة محرك',
      'Driving habits Analysis': 'تحليل عادات القيادة',
      'Over speed duration': 'خلال مدة السرعة',
      'Fatigue driving duration': 'مدة القيادة التعب',
      'High G force instance': 'ارتفاع سبيل المثال قوة G',
      'Harsh braking instance': 'المثال الكبح القاسي',
      'Acceleration instance': 'المثال تسريع',
      'Harsh acceleration instance': 'المثال تسارع قاسية',
      'CarOnline.Guru': 'CarOnline.Guru',
      'Check': 'الاختيار',
      'Location': 'موقع',
      'Avg oil consume': 'تستهلك النفط متوسط',
      'Number driving': 'عدد القيادة',
      'Next maintenance': 'صيانة المقبلة',
      'Health Check': 'فحص صحي',
      'Retry': 'إعادة المحاولة',
      'Description': 'وصف',
      'Value': 'القيمة',
      'Range': 'نطاق',
      'Username': 'اسم المستخدم',
      'password': 'كلمه السر',
      'Account Information': 'معلومات الحساب',
      'Old': 'قديم',
      'New': 'جديد',
      'Old Password': 'كلمة المرور القديمة',
      'Confirm Pasword': 'تأكيد Pasword',
    });


    $translateProvider.preferredLanguage('en');
  });
