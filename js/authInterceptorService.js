﻿'use strict';
 angular
    .module('obd').service('authInterceptorService', ['$q', '$injector','$location', 'localStorageService','$rootScope', function ($q, $injector,$location, localStorageService,$rootScope) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};
       
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            var authService = $injector.get('authService');
            var authData = localStorageService.get('authorizationData');

            if (authData) {
                if (authData.useRefreshTokens) {
                  /*  $location.path('/refresh');*/
				   authService.refreshToken().then(function (response) {
						return $q.reject(rejection);
					},
					 function (err) {
						$rootScope.Showlogin();
					 });
                    
                }
            }
            authService.logOut();
            $location.path('/home');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);