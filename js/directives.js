'use strict';

angular.module('obd')
    .directive('mygooglemap', mygooglemap);

function mygooglemap() {
    return {
      
         templateUrl: 'templates/GoogleMapRoute.html',
		controller:'GoogleCtrl'

    }

}

 angular
    .module('obd')
    .directive('mydevices', mydevices);

function mydevices() {
    return {
      
         templateUrl: 'templates/menu.html',
		controller:'AppCtrl'

    }

}  


 angular
    .module('obd')   
    .directive('mydashboad', mydashboad);
function mydashboad() {
    return {
         templateUrl: 'templates/root.html',
    }

}

 angular
    .module('obd')   
    .directive('mytrips', mytrips);
function mytrips() {
    return {
         templateUrl: 'templates/trip.html',
    }

}


 angular
    .module('obd')   
    .directive('mypersonalcenter', mypersonalcenter);
function mypersonalcenter() {
    return {
         templateUrl: 'templates/personal-center.html',
    }

}


angular
    .module('obd')   
    .directive('mysettings', mysettings);
function mysettings() {
    return {
         templateUrl: 'templates/settings.html',
    }

}
